import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _d1eaa170 = () => interopDefault(import('../pages/home.vue' /* webpackChunkName: "pages/home" */))
const _68302050 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _1d39fd21 = () => interopDefault(import('../pages/parent_feedback.vue' /* webpackChunkName: "pages/parent_feedback" */))
const _2b80d69c = () => interopDefault(import('../pages/Sign-up.vue' /* webpackChunkName: "pages/Sign-up" */))
const _c2ef058e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/home",
    component: _d1eaa170,
    name: "home"
  }, {
    path: "/login",
    component: _68302050,
    name: "login"
  }, {
    path: "/parent_feedback",
    component: _1d39fd21,
    name: "parent_feedback"
  }, {
    path: "/Sign-up",
    component: _2b80d69c,
    name: "Sign-up"
  }, {
    path: "/",
    component: _c2ef058e,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
